drop table if exists alextra;
drop table if exists transaccion;
drop table if exists alerta;
drop table if exists oficina;
drop table if exists ciudad;
drop table if exists usuario;
drop table if exists departamento;
drop table if exists tipotrx;
drop table if exists respuesta;
drop table if exists referencia;
drop table if exists categoria;
drop table if exists dependencia;

create table usuario(
    usuId int,
    usuNom varchar(40), 
    usuApe varchar(40),
    usuEmail varchar(60),
    usuPass varchar(20) );

Create table departamento(
    depId int,
    depNom varchar(40));

Create table ciudad(
    ciuId int,
    ciuNom varchar(40),
    ciuDep int );

create table oficina(
    ofiId varchar(20),
    ofiNom varchar(60),
    ofiCiu int );

create table tipoTrx(
    tipId int,
    tipNom varchar(20) );    

create table respuesta(
    resId int,
    resNom varchar(20) );        

create table referencia(
    refId int,
    refNom varchar(20) );        

create table transaccion(
    traId int AUTO_INCREMENT,
    traOfiOri varchar(20),
    traOfiDes varchar(20),
    traTip int,
    traRef int,
    traMarcaReq datetime,
    traMarcaRes datetime );

create table dependencia(
    dpeId int,
    dpeNom varchar(20) );

create table categoria(
    catId int,
    catNom varchar(20) );

create table alerta(
    alerId int AUTO_INCREMENT, 
    aleNivel int,
    aleDep int,
    aleCat int );

create table alextra(
    axttra int,
    axtale int );

ALTER TABLE usuario ADD CONSTRAINT PK_usu PRIMARY KEY (usuId);
ALTER TABLE departamento ADD CONSTRAINT PK_dep PRIMARY KEY (depId);
ALTER TABLE ciudad ADD CONSTRAINT PK_ciu PRIMARY KEY (ciuId);
ALTER TABLE oficina ADD CONSTRAINT PK_ofi PRIMARY KEY (ofiId);
ALTER TABLE tipoTrx ADD CONSTRAINT PK_tip PRIMARY KEY (tipId);
ALTER TABLE respuesta ADD CONSTRAINT PK_res PRIMARY KEY (resId);
ALTER TABLE referencia ADD CONSTRAINT PK_ref PRIMARY KEY (refId);
ALTER TABLE transaccion ADD CONSTRAINT PK_tra PRIMARY KEY (traId);
ALTER TABLE dependencia ADD CONSTRAINT PK_dpe PRIMARY KEY (dpeId);
ALTER TABLE categoria ADD CONSTRAINT PK_cat PRIMARY KEY (catId);
ALTER TABLE alerta ADD CONSTRAINT PK_ale PRIMARY KEY (aleId);
ALTER TABLE alextra ADD CONSTRAINT PK_axt PRIMARY KEY (axtTra, axtale);

ALTER TABLE ciudad ADD CONSTRAINT FK_ciudep FOREIGN KEY (ciuDep) REFERENCES departamento (depid) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE oficina ADD CONSTRAINT FK_oficiu FOREIGN KEY (oficiu) REFERENCES ciudad (ciuid) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE transaccion ADD CONSTRAINT FK_traofiori FOREIGN KEY (traofiori) REFERENCES oficina (ofiid) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE transaccion ADD CONSTRAINT FK_traofides FOREIGN KEY (traofides) REFERENCES oficina (ofiid) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE transaccion ADD CONSTRAINT FK_tratip FOREIGN KEY (tratip) REFERENCES tipotrx (tipid) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE transaccion ADD CONSTRAINT FK_trares FOREIGN KEY (trares) REFERENCES respuesta (resid) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE transaccion ADD CONSTRAINT FK_traref FOREIGN KEY (traref) REFERENCES referencia (refid) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE alerta ADD CONSTRAINT FK_aledep FOREIGN KEY (aledep) REFERENCES dependencia (depid) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE alerta ADD CONSTRAINT FK_alecat FOREIGN KEY (alecat) REFERENCES categoria (catid) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE alextra ADD CONSTRAINT FK_axttra FOREIGN KEY (axttra) REFERENCES transaccion (traid) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE alextra ADD CONSTRAINT FK_axtale FOREIGN KEY (axtale) REFERENCES alerta (aleid) ON UPDATE CASCADE ON DELETE RESTRICT;
